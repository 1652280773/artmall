import updateManager from './common/updateManager';
App({
  onLaunch: function () {
    wx.clearStorage({})
  },
  onShow: function () {
    updateManager();
  },
});
