import { getCategoryList } from '../../../services/good/fetchCategoryList';
Page({
  data: {
    list: [],
  },
  async init() {
    try {
      // const result = await getCategoryList();
      // this.setData({
      //   list: result,
      // });
      let that = this
      wx.request({
        url: 'http://47.104.233.102/api/product/category/list/collect/classify',
        method:'GET',
        data:{},
        success(res){
          const result1 = res.data.data.map((item)=>{
            return {
              groupId:item.catId,
              name:item.name,
              thumbnail:"https://cdn-we-retail.ym.tencent.com/miniapp/category/category-default.png",
              children:[
                {
                  groupId:item.catId,
                  name:item.name,
                  thumbnail:"https://cdn-we-retail.ym.tencent.com/miniapp/category/category-default.png",
                  children:item.children
                }
              ]
            }
          })
          // const result2 = result1.reduce((item)=>{
          //   item.children[0].children.reduce((item1)=>{
          //     item1.groupId = item1.catId
          //     item1.thumbnail = "https://pic42.photophoto.cn/20170306/1155117175516121_b.jpg"
          //   })
          // })
          that.setData({
            list:result1
          })
        }
      })
    } catch (error) {
      console.error('err:', error);
    }
  },

  onShow() {
    this.getTabBar().init();
  },
  onChange() {
    wx.navigateTo({
      url: '/pages/goods/list/index',
    });
  },
  onLoad() {
    this.init(true);
  },
});
