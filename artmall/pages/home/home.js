import { fetchHome } from '../../services/home/home';
import { fetchGoodsList } from '../../services/good/fetchGoods';
import Toast from 'tdesign-miniprogram/toast/index';
wx.cloud.init({env: 'cloud1-6gk91wbx557b6948'})
const db = wx.cloud.database()
Page({
  data: {
    imgSrcs: [],
    tabList: [
      {
        text: '精选推荐',
        key: 0,
      },
      // {
      //   text: '夏日防晒',
      //   key: 1,
      // },
      // {
      //   text: '二胎大作战',
      //   key: 2,
      // },
      // {
      //   text: '人气榜',
      //   key: 3,
      // },
      // {
      //   text: '好评榜',
      //   key: 4,
      // },
      // {
      //   text: 'RTX 30',
      //   key: 5,
      // },
      // {
      //   text: '手机也疯狂',
      //   key: 6,
      // },
    ],
    goodsList: [],
    goodsListLoadStatus: 0,
    pageLoading: false,
    current: 1,
    autoplay: true,
    duration: 500,
    interval: 5000,
    navigation: { type: 'dots' },
  },

  goodListPagination: {
    index: 0,
    num: 20,
  },

  privateData: {
    tabIndex: 0,
  },

  onShow() {
    this.getTabBar().init();
  },

  onLoad() {
    this.init();
  },

  onReachBottom() {
    if (this.data.goodsListLoadStatus === 0) {
      this.loadGoodsList();
    }
  },

  onPullDownRefresh() {
    this.init();
  },

  init() {
    this.loadHomePage();
  },

  loadHomePage() {
    wx.stopPullDownRefresh();

    this.setData({
      pageLoading: true,
    });
    // fetchHome().then(({ swiper, tabList }) => {
    //   this.setData({
    //     tabList,
    //     imgSrcs: swiper,
    //     pageLoading: false,
    //   });
    //   this.loadGoodsList(true);
    // });
    db.collection('swiper')
    .field({
      _id:false,
      img:true,
      text: true,
    })
    .get()
    .then(res=>{
      this.setData({
        imgSrcs:res.data,
        pageLoading: false,
      })
      this.loadGoodsList(true);
    })
    
  },

  tabChangeHandle(e) {
    this.privateData.tabIndex = e.detail;
    this.loadGoodsList(true);
  },

  onReTry() {
    this.loadGoodsList();
  },

  async loadGoodsList(fresh = false) {
    if (fresh) {
      wx.pageScrollTo({
        scrollTop: 0,
      });
    }

    this.setData({ goodsListLoadStatus: 1 });

    const pageSize = this.goodListPagination.num;
    let pageIndex =
      this.privateData.tabIndex * pageSize + this.goodListPagination.index + 1;
    if (fresh) {
      pageIndex = 0;
    }

    try {
      // const nextList = await fetchGoodsList(pageIndex, pageSize);
      // this.setData({
      //   goodsList: fresh ? nextList : this.data.goodsList.concat(nextList),
      //   goodsListLoadStatus: 0,
      // });
      let that = this
      wx.request({
        url: 'http://47.104.233.102/api/product/spu/list/tree',
        method: 'POST',
        data:{
          limit:100
        },
        success(res){
          const nextList = res.data.page.list.map((item)=>{
            return {
              spuId:item.spuId,
              originPrice:item.price*100,
              price:item.price*100,
              thumb:'http://39.104.17.126:8080/ipfs/'+item.imgMediaUrl,
              title:item.spuName+"\xa0"+"\xa0"+"\xa0"+"\xa0"+"\xa0"+"\xa0"+"\xa0"+"\xa0"+"\xa0"+"\xa0"+"\xa0"+"\xa0"+"\xa0"+"\xa0",
              tags:"数字藏品"
            }
          })
          that.setData({
          goodsList: fresh ? nextList : that.data.goodsList.concat(nextList),
          goodsListLoadStatus: 0,
        });
        }
      })
      this.goodListPagination.index = pageIndex;
      this.goodListPagination.num = pageSize;
    } catch (err) {
      this.setData({ goodsListLoadStatus: 3 });
    }
  },

  goodListClickHandle(e) {
    const { index } = e.detail;
    const { spuId } = this.data.goodsList[index];
    wx.navigateTo({
      url: `/pages/goods/details/index?spuId=${spuId}`,
    });
  },

  goodListAddCartHandle() {
    Toast({
      context: this,
      selector: '#t-toast',
      message: '点击加入购物车',
    });
  },

  navToSearchPage() {
    wx.navigateTo({ url: '/pages/goods/search/index' });
  },

  navToActivityDetail({ detail }) {
    const { index: promotionID = 0 } = detail || {};
    wx.navigateTo({
      url: `/pages/promotion-detail/index?promotion_id=${promotionID}`,
    });
  },
});
