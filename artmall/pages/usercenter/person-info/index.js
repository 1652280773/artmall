import { fetchPerson } from '../../../services/usercenter/fetchPerson';
import { phoneEncryption } from '../../../utils/util';
import Toast from 'tdesign-miniprogram/toast/index';
import JSEncrypt from 'nodejs-jsencrypt'
Page({
  data: {
    personInfo: {
      avatarUrl: '',
      nickName: '',
      gender: 0,
      phoneNumber: '',
    },
    showUnbindConfirm: false,
    pickerOptions: [
      {
        name: '男',
        code: '1',
      },
      {
        name: '女',
        code: '2',
      },
    ],
    typeVisible: false,
    genderMap: ['', '男', '女'],
  },
  onLoad() {
    // this.init();
    this.setData({
      personInfo:{
        avatarUrl:wx.getStorageSync('avatarUrl'),
        nickName:wx.getStorageSync('nickName'),
        phoneNumber:phoneEncryption(wx.getStorageSync('phonenumber'))
      }
    })
  },
  onShow(){
    this.setData({
      personInfo:{
        avatarUrl:wx.getStorageSync('avatarUrl'),
        nickName:wx.getStorageSync('nickName'),
        phoneNumber:phoneEncryption(wx.getStorageSync('phonenumber'))
      }
    })
  },
  init() {
    this.fetchData();
  },
  fetchData() {
    fetchPerson().then((personInfo) => {
      this.setData({
        personInfo,
        'personInfo.phoneNumber': phoneEncryption(personInfo.phoneNumber),
      });
    });
  },
  onClickCell({ currentTarget }) {
    const { dataset } = currentTarget;
    const { nickName } = this.data.personInfo;

    switch (dataset.type) {
      case 'gender':
        this.setData({
          typeVisible: true,
        });
        break;
      case 'name':
        wx.navigateTo({
          url: `/pages/usercenter/name-edit/index?name=${nickName}`,
        });
        break;
      case 'avatarUrl':
        this.toModifyAvatar();
        break;
      case 'phoneNumber':
        wx.navigateTo({
          url: `/pages/usercenter/phonenumber-edit/index`,
        })
      default: {
        break;
      }
    }
  },
  onClose() {
    this.setData({
      typeVisible: false,
    });
  },
  onConfirm(e) {
    const { value } = e.detail;
    this.setData(
      {
        typeVisible: false,
        'personInfo.gender': value,
      },
      () => {
        Toast({
          context: this,
          selector: '#t-toast',
          message: '设置成功',
          theme: 'success',
        });
      },
    );
  },
  async toModifyAvatar() {
    try {
      const tempFilePath = await new Promise((resolve, reject) => {
        wx.chooseImage({
          count: 1,
          sizeType: ['compressed'],
          sourceType: ['album', 'camera'],
          success: (res) => {
            const { path, size } = res.tempFiles[0];
            if (size <= 10485760) {
              resolve(path);
            } else {
              reject({ errMsg: '图片大小超出限制，请重新上传' });
            }
          },
          fail: (err) => reject(err),
        });
      });
      const tempUrlArr = tempFilePath.split('/');
      const tempFileName = tempUrlArr[tempUrlArr.length - 1];
      Toast({
        context: this,
        selector: '#t-toast',
        message: `已选择图片-${tempFileName}`,
        theme: 'success',
      });
    } catch (error) {
      if (error.errMsg === 'chooseImage:fail cancel') return;
      Toast({
        context: this,
        selector: '#t-toast',
        message: error.errMsg || error.msg || '修改头像出错了',
        theme: 'fail',
      });
    }
  },
  openUnbindConfirm:function(){
    let that = this
    wx.getUserProfile({
      desc: '请授权登录',
      success(res){
        that.setData({
          personInfo:{
            nickName:res.userInfo.nickName,
            avatarUrl:res.userInfo.avatarUrl,
            gender:res.userInfo.gender,
          }
        })
        wx.setStorageSync('nickName', res.userInfo.nickName)
        wx.setStorageSync('avatarUrl', res.userInfo.avatarUrl)
        wx.setStorageSync('gender', res.userInfo.gender)
        wx.login({
          success: function(res1) {
            if (res1.code) {
              wx.request({
                url: 'https://api.weixin.qq.com/sns/jscode2session',
                data:{
                  'appid':'wx715c774dbe99b3bb',
                  'secret':'777b0cb0a285e39059c8394baff987bb',
                  'js_code':res1.code,
                  'grant_type':'authorization_code'
                },
                success: function(response){
                  wx.setStorageSync('session_key', response.data.session_key)
                  wx.setStorageSync('openId', response.data.openid)
                  const data = {
                    avatar:res.userInfo.avatarUrl,
                    openId:response.data.openid,
                    username:res.userInfo.nickName
                  }
                  // var encrypt = new JSEncrypt()
                  // encrypt.setPublicKey("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCue/mEwIIRKSo9ITKJ0IZ/+sInTW6LvLR/HkaM5kEygejC5eD6sQYHrj2AnoDyrQ8PW3LYzLAyBbH1XiZ4PycddBi9yw/WZ83skQDTXts7GVy/eAerUDaUuXy+DK9qpqUyfmsaXSiTX8BDBwfggKk2cLF2enR/UdNw8yboxd3KawIDAQAB")
                  // var encrypted = encrypt.encrypt(data)
                  // console.log(encrypted)
                  // var decrypt = new JSEncrypt()
                  // decrypt.setPrivateKey("MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAK57+YTAghEpKj0hMonQhn/6widNbou8tH8eRozmQTKB6MLl4PqxBgeuPYCegPKtDw9bctjMsDIFsfVeJng/Jx10GL3LD9ZnzeyRANNe2zsZXL94B6tQNpS5fL4Mr2qmpTJ+axpdKJNfwEMHB+CAqTZwsXZ6dH9R03DzJujF3cprAgMBAAECgYAoy63EXypKDuDkWA2z8UBxTdT1UYJmQ6HEEvUV3oQh5gPhBdR7aMZrJ08T8ntdwh7CTXRlwj30BCbeorvF46YmNjbwHgTMCksU6PoXFiSCdlKUc3LKSw4RR/9avw1bv+PpaoA1WOOG9+I5xgvKV0cJYn40D+AKoEG596Sfncx7eQJBAO808cF6EqC4F/DxspbGbysUZbyDDsi/Lu+7pwm6W/FhILL4s/KUlJQ/cIXKrlkp69ZFD6uCNCV6ws2+j3MprYcCQQC6u9Qbi9+PTIJGoIKyZwSWM/0TU1STONdglyBcUh5GVnEmn21/mPWqhUFeC3yD/0Fa/oLOz6KYc3DkdT4Im1T9AkEAlWZX6CM6Jy9TpKUQiaL+I8XllMu2HCNsSUbGmQjQC/5B0gYLPeMWuVC2Epn4MrBAH9+aXeBAeXgm2yG9rovFWQJAIvU43zNPVfdfyDMSZK2uiSQQc8a85ZbVk0qXj1D8Q/S42B8uk52iGBs8Qovp8pgdtxhAitEKQnoSH2LnixrxTQJBANS072aqTfrTo1Gej/HneOhlKYxay+/b4GDTDlptKduCTm7xJDYWsqYjm4puQUhHkInaim2NaW2ZTpqces2XD/4=")
                  // var unencrypted = decrypt.decrypt(encrypted)
                  // console.log(unencrypted)
                  var encrypt = new JSEncrypt();
                  encrypt.setPublicKey(
                    "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCue/mEwIIRKSo9ITKJ0IZ/+sInTW6LvLR/HkaM5kEygejC5eD6sQYHrj2AnoDyrQ8PW3LYzLAyBbH1XiZ4PycddBi9yw/WZ83skQDTXts7GVy/eAerUDaUuXy+DK9qpqUyfmsaXSiTX8BDBwfggKk2cLF2enR/UdNw8yboxd3KawIDAQAB"
                  );
                  var encrypted = encrypt.encrypt(JSON.stringify(data));
                  wx.request({
                    url: 'http://47.104.233.102/api/user/user/loginMiniProgram',
                    method:'POST',
                    data:encrypted,
                    success(res){
                      wx.setStorageSync('token', res.data.data.token)
                    }
                  })
                }
              })
            } else {
              console.log('获取用户登录态失败！' + res.errMsg)
            }
          },
          fail: function(){
            console.log("启用wx.login函数，失败！");
          },
          complete:function(){
            console.log("已启用wx.login函数");
          }
          
        });
      }
    })
  }
});
