// pages/usercenter/phonenumber-edit/index.js
const app = getApp()
Page({
  data: {
    phonenumber:''
  },
  onLoad(){
    this.setData({
      phonenumber:wx.getStorageSync('phonenumber')
    })
  },
  onShow(){
    this.setData({
      phonenumber:wx.getStorageSync('phonenumber')
    })
  },
  onSubmit:function(){
    wx.navigateBack({ backRefresh: true });
    wx.setStorageSync('phonenumber', this.data.phonenumber)
  },
})