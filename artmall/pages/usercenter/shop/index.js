// pages/usercenter/shop/index.js
wx.cloud.init()
const db = wx.cloud.database()
Page({
  data: {
    value:0,
    name:"",
    phone:"",
    price:"",
    detail:"",
    goodId:"",
    originFiles:"",
    gridConfig:"",
    fileList:[]
  },
  tabChangeHandle(e) {
    this.setData({
      value:e.detail.value
    })
  },
  deleteClick(event) {
    var imgData = this.data.fileList;
    // 通过splice方法删除图片
    imgData.splice(event.detail.index, 1);
    // 更新图片数组
    this.setData({
        fileList: imgData
    })
  },
  afterRead(event) {
    // loading加载
    wx.showLoading({
        title: '上传中...'
    });
    const {file} = event.detail;//获取图片详细信息
    let that = this;//防止this指向问题
      // 将图片上传至云存储空间
    wx.cloud.uploadFile({
      // 指定上传到的云路径
      cloudPath: "fileImage/" + new Date().getTime() + "_" +  Math.floor(Math.random()*1000) + ".jpg",
      // 指定要上传的文件的小程序临时文件路径
      filePath: file.url,
      // 成功回调
      success: res => {
        that.data.fileList.push({...file})
        that.setData({
          fileList:this.data.fileList,
          goodId:res.fileID
        })
        wx.hideLoading()
      }
    })
  },
  formSubmit(){
    wx.navigateBack({ backRefresh: true });
    db.collection('goods').add({
      data:{
        name:this.data.name,
        phone:this.data.phone,
        price:this.data.price,
        detail:this.data.detail,
        goodId:this.data.goodId
      },
      success(res){
        console.log(res)
      }
    })
  },
})