export function stop(ms){
  let s = new Date().getTime();
  while(new Date().getTime()-s<ms){}
  return true;
}